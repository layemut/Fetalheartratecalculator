package com.example.ozcan.fhrandroid;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.sql.SQLException;


public class Login extends Activity {
    EditText name, password;
    Button login, register,list;
    ImageView logo;
    Database databaseAdapter = new Database(this);

    public void setButtons() {
        name = (EditText) findViewById(R.id.editTextName);
        password = (EditText) findViewById(R.id.editTextPassword);
        login = (Button) findViewById(R.id.buttonLogin);
        register = (Button) findViewById(R.id.buttonRegister);
        logo=(ImageView)findViewById(R.id.logoView);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setButtons();

        Animation logoAnim = AnimationUtils.loadAnimation(this, R.anim.logoanimation);
        logo.setAnimation(logoAnim);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    databaseAdapter.open();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (databaseAdapter.isAvaible(name.getText().toString(), password.getText().toString())) {
                    success();
                    String file = databaseAdapter.getFileNameByName(name.getText().toString(),
                            password.getText().toString());
                    Intent i = new Intent(Login.this, MainActivity.class);
                    i.putExtra("fileset", file);
                    startActivity(i);

                } else {
                    fail();
                }
                databaseAdapter.close();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Register.class);
                startActivity(i);
            }
        });


    }

    public void success() {
        Toast.makeText(this, "Başarılı!", Toast.LENGTH_SHORT).show();
    }

    public void fail() {
        Toast.makeText(this, "Başarısız!", Toast.LENGTH_SHORT).show();
    }


}
