package com.example.ozcan.fhrandroid;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class DccFragment extends Fragment {
    ListView list;
    public DccFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_dcc,container,false);
        list=(ListView)layout.findViewById(R.id.dccList);
        TextView header = new TextView(getActivity().getBaseContext());

        header.setText("Decelerations");
        header.setTextColor(Color.BLACK);
        header.setGravity(Gravity.CENTER_HORIZONTAL);
        list.addHeaderView(header);

        String[] dccArray = new String[((MainActivity)getActivity()).dccCounter];
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity().getBaseContext(),
                android.R.layout.simple_list_item_1,((MainActivity)getActivity()).findDcc());
        list.setAdapter(adapter);

        return layout;
    }
}
