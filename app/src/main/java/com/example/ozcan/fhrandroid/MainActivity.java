package com.example.ozcan.fhrandroid;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;


public class MainActivity extends FragmentActivity {
    ViewPager pages;

    int time = 0, accCounter = 0, dccCounter = 0, bvCounter = 0;
    int pulseNumber = 0, skipFirstLine = 0, week = 35;

    int[] accStartPoint = new int[600];
    int[] ucStartPoint = new int[600];
    int[] ucFinishPoint = new int[600];
    int[] accFinishPoint = new int[600];
    int[] dccStartPoint = new int[600];
    int[] dccFinishPoint = new int[600];
    int[] bvStartPoint = new int[600];
    int[] bvFinishPoint = new int[600];
    String[] bvType = new String[600];
    double[] accDuration = new double[600];
    double[] dccDuration = new double[600];
    double[] bvDuration = new double[600];

    double accPeakValue[] = new double[600];
    double bvPeakValue[] = new double[600];
    double dccPeakValue[] = new double[600];

    double baseline = 0;
    double[] pulse = new double[20000];
    double[] uc = new double[20000];
    double[] baselineGraph = new double[20000];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pages = (ViewPager) findViewById(R.id.pager);
        FragmentManager fragmentManager = getSupportFragmentManager();
        pages.setAdapter(new pagerAdapter(fragmentManager));
        new async().execute();
    }

    public void readFile(String fileName) {

        final String DELIMITER = ",";
        BufferedReader fileReader = null;
        try {
            String line = "";
            double tempAccSum = 0, tempDcSum = 0;
            int tempCounter = 0;

            InputStream iS = getResources().getAssets().open(fileName + ".csv");
            fileReader = new BufferedReader(new InputStreamReader(iS));

            while ((line = fileReader.readLine()) != null) {

                skipFirstLine++;
                if (skipFirstLine == 1) {
                    continue;
                }

                String[] fhr = line.split(DELIMITER);
                if (Double.parseDouble(fhr[1]) != 0) {
                    tempAccSum += Double.parseDouble(fhr[1]);
                    tempDcSum += Double.parseDouble(fhr[2]);
                    tempCounter++;
                    if (tempCounter > 2) {
                        pulse[pulseNumber] = tempAccSum / tempCounter;
                        uc[pulseNumber] = tempDcSum / tempCounter;
                        tempCounter = 0;
                        tempAccSum = 0;
                        tempDcSum = 0;
                        pulseNumber++;
                    }
                } else if (Double.parseDouble(fhr[1]) == 0) {
                    tempAccSum += setMissingValue(pulseNumber);
                    tempDcSum += Double.parseDouble(fhr[2]);
                    tempCounter++;
                    if (tempCounter > 2) {
                        pulse[pulseNumber] = tempAccSum / tempCounter;
                        uc[pulseNumber] = tempDcSum / tempCounter;
                        tempCounter = 0;
                        tempAccSum = 0;
                        tempDcSum = 0;
                        pulseNumber++;
                    }
                }
            }

        } catch (Exception e) {
            throw new RuntimeException("hata 1");
        }finally {
            findBaseline();
            try {
                fileReader.close();
            } catch (IOException e) {
                throw new RuntimeException("hata 2");
            }
        }

    }

    public String[] findAcc() {
        time = 0;
        accCounter = 0;
        bvCounter=0;
        accPeakValue[0] = pulse[0];

        for (int a = 0; a < pulseNumber; a++) {
            //nabız baseline'dan yüksek olduğu anda
            //zamanı tutmaya başlıyoruz, zaman sıfırsa
            //acc ve bv için başlangıç noktaları ve
            //peak'i hesaplamak için peak'e değerleri veriyoruz
            if (pulse[a] > baseline) {
                if (time == 0) {
                    accStartPoint[accCounter] = a;
                    bvStartPoint[bvCounter] = a;
                    accPeakValue[accCounter] = pulse[a];
                    bvPeakValue[bvCounter] = pulse[a];
                }

                time++;

                if (pulse[a] > accPeakValue[accCounter]) {
                    accPeakValue[accCounter] = pulse[a];
                    bvPeakValue[bvCounter] = pulse[a];
                }
            }

            if (week < 32) {
                //zaman 10 saniyeden fazla ve nabız baseline'a gelip
                //peak noktası baseline'dan 10 bpm fazlaysa
                //acc'yi hesaplıyoruz, bitiş noktası ve süresi.
                if (time > 10 && (pulse[a] <= (baseline + 5))
                        && (accPeakValue[accCounter] - baseline) > 10) {

                    accDuration[accCounter] = time;
                    accFinishPoint[accCounter] = a;

                    //Eğer zaman 120'den büyükse aynı işlemi bv için yapıyoruz
                    if (time >= 120) {
                        bvDuration[bvCounter] = time;
                        bvFinishPoint[bvCounter] = a;

                        if (bvPeakValue[bvCounter] - baseline < 5) {
                            bvType[bvCounter] = "Minimal";
                        } else if (bvPeakValue[bvCounter] - baseline == 0) {
                            bvType[bvCounter] = "Absent";
                        } else if (bvPeakValue[bvCounter] - baseline >= 5
                                && bvPeakValue[bvCounter] - baseline < 26) {
                            bvType[bvCounter] = "Moderate";
                        } else if (bvPeakValue[bvCounter] - baseline >= 26) {
                            bvType[bvCounter] = "Marked";
                        }
                        bvCounter++;
                    } else {
                        accCounter++;
                    }
                    time = 0;
                } else if (pulse[a] <= (baseline + 5)) {
                    time = 0;
                }
            } else if (week >= 32) {
                if (time > 15 && (pulse[a] <= (baseline + 5))
                        && (accPeakValue[accCounter] - baseline) > 15) {

                    accDuration[accCounter] = time;
                    accFinishPoint[accCounter] = a;

                    if (time >= 120) {
                        bvDuration[bvCounter] = time;
                        bvFinishPoint[bvCounter] = a;

                        if (bvPeakValue[bvCounter] - baseline < 5) {
                            bvType[bvCounter] = "Minimal";
                        } else if (bvPeakValue[bvCounter] - baseline == 0) {
                            bvType[bvCounter] = "Absent";
                        } else if (bvPeakValue[bvCounter] - baseline >= 5
                                && bvPeakValue[bvCounter] - baseline < 26) {
                            bvType[bvCounter] = "Moderate";
                        } else if (bvPeakValue[bvCounter] - baseline >= 26) {
                            bvType[bvCounter] = "Marked";
                        }
                        bvCounter++;
                    } else {
                        accCounter++;
                    }
                    time = 0;
                } else if (pulse[a] <= (baseline + 5)) {
                    time = 0;
                }
            }
        }

        String[] accInformation = new String[accCounter];
        for (int a = 0; a < accCounter; a++) {
            accInformation[a] = (a + 1) + ". Acceleration, Duration: "
                    + accDuration[a] + ", Peak: " + Math.floor(accPeakValue[a])
                    + ", Start: " + accStartPoint[a] / 60 + " Minute(s) "
                    + accStartPoint[a] % 60 + " Second(s) "
                    + ", Finish: " + accFinishPoint[a] / 60 + " Minute(s) "
                    + accFinishPoint[a] % 60 + " Second(s)";
        }
        return accInformation;
    }

    public void findBaseline(){
        for(int a=0;a<pulseNumber;a++){
            baseline+=pulse[a];
        }
        baseline=baseline/pulseNumber;
        for(int a=0;a<pulseNumber;a++){
            baselineGraph[a]=baseline;
        }
    }

    public String[] findDcc() {
        time = 0;
        dccCounter = 0;
        dccPeakValue[0] = pulse[0];
        for (int a = 0; a < pulseNumber; a++) {
            if (pulse[a] < baseline) {
                if (time == 0) {
                    dccStartPoint[dccCounter] = a;
                    dccPeakValue[dccCounter] = pulse[a];
                }
                time++;
                if (pulse[a] < dccPeakValue[dccCounter]) {
                    dccPeakValue[dccCounter] = pulse[a];
                }
            }

            if (week < 32) {
                if (time > 20 && (pulse[a] >= (baseline - 5))
                        && (baseline - dccPeakValue[dccCounter]) > 15) {
                    dccDuration[dccCounter] = time;
                    dccFinishPoint[dccCounter] = a;
                    time = 0;
                    dccCounter++;
                } else if (pulse[a] > (baseline - 5)) {
                    time = 0;
                }
            } else if (week >= 32) {
                if (time > 25 && (pulse[a] >= (baseline - 5))
                        && (baseline - dccPeakValue[dccCounter]) > 15) {
                    dccDuration[dccCounter] = time;
                    dccFinishPoint[dccCounter] = a;
                    dccCounter++;
                    time = 0;
                } else if (pulse[a] > (baseline - 5)) {
                    time = 0;
                }
            }
        }
        String[] dccInformation = new String[dccCounter];
        for (int a = 0; a < dccCounter; a++) {
            dccInformation[a] = (a + 1) + ". Deceleration, Duration: "
                    + dccDuration[a] + ", Peak: " + Math.floor(dccPeakValue[a])
                    + ", Start: " + dccStartPoint[a] / 60 + " Minute(s) "
                    + dccStartPoint[a] % 60 + " Second(s) "
                    + ", Finish: " + dccFinishPoint[a] / 60 + " Minute(s) "
                    + dccFinishPoint[a] % 60 + " Second(s)";
        }
        return dccInformation;
    }

    public String[] findBV(){
        String[] bvInformation = new String[bvCounter];
        for(int a=0;a<bvCounter;a++){
            bvInformation[a]=(a+1)+". Baseline Variability, Type:"
                    +bvType[a]+", Duration: "+bvDuration[a]
                    +", Peak: "+Math.floor(bvPeakValue[a])
                    +", Start: "+bvStartPoint[a]/60+" Minute(s) "
                    +bvStartPoint[a]%60+" Second(s) "
                    +", Finish: "+bvFinishPoint[a]/60+" Minute(s) "
                    +bvFinishPoint[a]%60+" Second(s)";
        }
        return bvInformation;
    }

    public double setMissingValue(int index) {
        if(index==0){
            return 140.0;
        }else{
            return new Random().nextDouble();
        }
    }

    class pagerAdapter extends FragmentPagerAdapter {

        public pagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragmentToDisplay = null;
            if (position == 0) {
                fragmentToDisplay = new GraphFragment();
            } else if (position == 1) {
                fragmentToDisplay = new AccFragment();
            } else if (position == 2) {
                fragmentToDisplay = new DccFragment();
            } else if (position == 3) {
                fragmentToDisplay = new BVFragment();
            }
            return fragmentToDisplay;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    class async extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            readFile(getIntent().getExtras().getString("fileset"));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

}
