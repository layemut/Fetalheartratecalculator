package com.example.ozcan.fhrandroid;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.SQLException;


public class Register extends Activity {
    Database databaseAdapter = new Database(this);
    EditText name, surname, password, week, setid;
    Button register;

    public void setButtons() {
        name = (EditText) findViewById(R.id.editTextName);
        surname = (EditText) findViewById(R.id.editTextSurname);
        password = (EditText) findViewById(R.id.editTextPassword);
        week = (EditText) findViewById(R.id.editTextWeek);
        setid = (EditText) findViewById(R.id.editTextSetid);
        register = (Button) findViewById(R.id.buttonRegister);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setButtons();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    databaseAdapter.open();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (name.getText().toString().length() > 1 && surname.getText().toString().length() > 1
                        && password.getText().toString().length() > 1 && week.getText().toString().length() > 1
                        && setid.getText().toString().length() > 1) {

                    databaseAdapter.Insert(name.getText().toString(), surname.getText().toString(),
                            password.getText().toString(), Integer.parseInt(week.getText().toString()),
                            Integer.parseInt(setid.getText().toString()));
                } else {
                    showFillLinesError();
                }
                databaseAdapter.close();

                Intent i = new Intent(Register.this, MainActivity.class);
                i.putExtra("fileset", setid.getText().toString());
                startActivity(i);
            }
        });
    }

    public void showFillLinesError() {
        Toast.makeText(this, "Lütfen tüm alanları doldurunuz!", Toast.LENGTH_SHORT).show();
    }

}
