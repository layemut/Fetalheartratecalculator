package com.example.ozcan.fhrandroid;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class BVFragment extends Fragment {
    ListView list;
    ArrayAdapter<String> adapter;

    public BVFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_bv, container, false);
        list = (ListView) layout.findViewById(R.id.bvList);

        String[] bvArray = new String[((MainActivity) getActivity()).bvCounter];
        bvArray = ((MainActivity) getActivity()).findBV();

        adapter = new ArrayAdapter<String>(getActivity().getBaseContext(),
                android.R.layout.simple_list_item_1, bvArray);
        list.setAdapter(adapter);

        TextView header = new TextView(getActivity().getBaseContext());
        header.setText("Variabilities");
        header.setTextColor(Color.BLACK);
        header.setGravity(Gravity.CENTER_HORIZONTAL);
        list.addHeaderView(header);

        list.invalidateViews();
        return layout;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
