package com.example.ozcan.fhrandroid;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;

public class Database {

    private static final String DATABASE_NAME = "Users";
    private static final String DATABASE_TABLE = "UserInformations";
    private static final int DATABASE_VERSION = 1;

    public static final String KEY_ROWID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_SURNAME = "surname";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_SETID = "setid";
    public static final String KEY_WEEK = "week";

    private DbHelper helper;
    private SQLiteDatabase database;
    private final Context context;

    public long Insert(String name, String surname, String password, int week, int setid) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, name);
        cv.put(KEY_SURNAME, surname);
        cv.put(KEY_PASSWORD, password);
        cv.put(KEY_WEEK, week);
        cv.put(KEY_SETID, setid);
        return database.insert(DATABASE_TABLE, null, cv);
    }

    public Cursor getAllRecords() {
        return database.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_NAME, KEY_SURNAME, KEY_PASSWORD, KEY_WEEK, KEY_SETID}
                , null, null, null, null, null);
    }

    public String getFileNameByName(String name, String password){
        String[] args={name,password};
        String where="name=? and password=?";
        Cursor c = database.query(DATABASE_TABLE,null,where,args,null,null,null);
        c.moveToFirst();
        return c.getString(5);
    }

    public boolean isAvaible(String name, String password) {
        String[] args={name,password};
        String where="name=? and password=?";
        Cursor c= database.query(DATABASE_TABLE,null,where,args,null,null,null);

        if (c.moveToFirst()) {
            return true;
        } else {
            return false;
        }
    }

    public Database(Context context) {
        this.context = context;
    }

    public Database open() throws SQLException {
        helper = new DbHelper(context);
        database = helper.getWritableDatabase();
        return this;
    }

    public void close() {
        helper.close();
    }

    private static class DbHelper extends SQLiteOpenHelper {
        public DbHelper(Context c) {
            super(c, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" +
                            KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            KEY_NAME + " TEXT NOT NULL, " +
                            KEY_SURNAME + " TEXT NOT NULL, " +
                            KEY_PASSWORD + " TEXT NOT NULL," +
                            KEY_WEEK + " INTEGER NOT NULL, " +
                            KEY_SETID + " INTEGER NOT NULL);"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(sqLiteDatabase);
        }
    }
}
