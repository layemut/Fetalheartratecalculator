package com.example.ozcan.fhrandroid;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

public class GraphFragment extends Fragment {
    LineChart fhrGraph;
    public GraphFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_graph,container,false);
        fhrGraph=(LineChart)layout.findViewById(R.id.chart);
        fhrGraph.setDescription("FHR graph");
        fhrGraph.setTouchEnabled(true);
        fhrGraph.setDragEnabled(true);
        fhrGraph.setScaleEnabled(true);
        fhrGraph.setPinchZoom(false);
        fhrGraph.setBackgroundColor(Color.LTGRAY);
        fhrGraph.animateY(500);
        setData(((MainActivity)getActivity()).pulse,((MainActivity)getActivity()).pulseNumber);
        fhrGraph.invalidate();
        return layout;
    }

    public void setData(double[] pulse, int lenght){
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < lenght; i++) {
            xVals.add((i) + "");
        }

        ArrayList<Entry> yVals2 = new ArrayList<>();
        for (int i = 0; i < lenght; i++) {
            float val = (float) ((MainActivity)getActivity()).baseline;
            yVals2.add(new Entry(val, i));
        }
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        for (int i = 0; i < lenght; i++) {
            float val = (float) pulse[i];
            yVals1.add(new Entry(val, i));
        }

        ArrayList<Entry> acc = new ArrayList<Entry>();
        ArrayList<Entry> dcc = new ArrayList<Entry>();
        ArrayList<Entry> bv = new ArrayList<Entry>();
        int e=0,d=0,t=0;
        for(int a=0;a<lenght;a++){
            if(((MainActivity)getActivity()).accStartPoint[e]==a){
                float val = (float) ((MainActivity)getActivity()).accPeakValue[e];
                for(int b=((MainActivity)getActivity()).accStartPoint[e];
                    b<((MainActivity)getActivity()).accFinishPoint[e];b++){

                }
            }
        }

        LineDataSet set1 = new LineDataSet(yVals1, "FHR");
        LineDataSet set2 = new LineDataSet(yVals2, "BASELINE");
        set2.setCircleSize(0);
        set2.setColor(Color.RED);
        set2.setLineWidth(1);
        set1.setLineWidth(1);
        set1.setColor(Color.BLUE);
        set1.setCircleSize(0);
        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1);
        dataSets.add(set2);

        LineData data = new LineData(xVals, dataSets);

        fhrGraph.setData(data);
    }
}
