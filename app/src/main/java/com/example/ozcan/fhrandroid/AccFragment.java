package com.example.ozcan.fhrandroid;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class AccFragment extends Fragment {
    ListView list;

    public AccFragment() {
        // Required empty public constructor
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_acc, container, false);
        list = (ListView)layout.findViewById(R.id.accList);

        TextView header = new TextView(getActivity().getBaseContext());
        header.setText("Accelerations");
        header.setTextColor(Color.BLACK);
        header.setGravity(Gravity.CENTER_HORIZONTAL);
        list.addHeaderView(header);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity().getBaseContext(),
                android.R.layout.simple_list_item_1,((MainActivity)getActivity()).findAcc());
        list.setAdapter(adapter);

        return layout;
    }


}
